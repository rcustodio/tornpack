import config

try:
    import tornpack_settings
except:
    from os import system
    system('make config')
    raise 

from tornpack.main import Main
import tornpack_services

Main().init()
