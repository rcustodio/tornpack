from google.protobuf.message import Message
from pika.spec import BasicProperties
from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key as RK
from tornpack.parser.json import jsonify
from tornpack.parser.protobuf import decode
from tornpack.options import options

__all__ = ['Kernel']

class Kernel(SimpleAsk):
    def __parse_json(self,name,body,future,headers):
        try:
            assert isinstance(body,dict)
        except AssertionError:
            pass
        except:
            raise
        else:
            body = jsonify(body)

        self.ioengine.ioloop.add_callback(
            self.__publish,
            name=name,
            body=body,
            future=future,
            headers=headers
        )
        return True

    def __parse_proto(self,name,body,future,headers):
        try:
            assert isinstance(body,Message)
        except AssertionError:
            self.ioengine.ioloop.add_callback(
                self.__parse_json,
                name=name,
                body=body,
                future=future,
                headers=headers
            )
        except:
            raise
        else:
            def on_encode(result):
                try:
                    assert result.result()
                except AssertionError:
                    self.ioengine.ioloop.add_callback(
                        self.future,
                        msg={'future':future,
                             'swap':{'future':False}}
                    )
                    raise
                except:
                    raise
                else:
                    self.ioengine.ioloop.add_callback(
                        self.__publish,
                        name=name,
                        body=result.result(),
                        future=future,
                        headers=headers
                    )
                return True

            self.__protobuf_encode__(
                body,
                self.ioengine.future_instance(on_encode)
            )
        return True

    def __publish(self,name,body,future,headers):
        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=future,
            body=body,
            routing_key=RK(name),
            headers=headers
        )
        return True

    def publish(self,name,body='',future=None,headers={},*args,**kwargs):
        try:
            assert isinstance(body,(str,unicode))
        except AssertionError:
            self.ioengine.ioloop.add_callback(
                self.__parse_proto,
                name=name,
                body=body,
                future=future,
                headers=headers
            )
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.__publish,
                name=name,
                body=body,
                future=future,
                headers=headers
            )
        return True
