from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy

__all__ = ['Remove']

class Remove(Base):
    @property
    def codes(self):
        return options.tornpack_njord_mongodb['codes']['remove']

    @property
    def name(self):
        return 'njord_mongodb_remove'

    @property
    def service(self):
        return 'remove'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,future,multi=False):
        def on_ask(result):
            try:
                assert self.codes['ok'] == result.result()['properties'].headers['code']
            except AssertionError:
                future.set_result(False)
            except:
                raise
            else:
                future.set_result(True)
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body=query,
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                ),
                'multi':multi
            }
        )
        return None
