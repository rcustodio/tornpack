from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.cursor.handler import Handler as Cursor
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy

__all__ = ['Find']

class Find(Base):
    @property
    def name(self):
        return 'njord_mongodb_find'

    @property
    def service(self):
        return 'find'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,future,fields=None,limit=10,skip=0,sort=None):
        def on_ask(result):
            try:
                body = dictfy(result.result()['body'])
                assert body
                future.set_result({'cursor':Cursor(body['service'],body['cursor']),'doc':body['doc']})
            except (AssertionError,AttributeError):
                future.set_result(False)
            except:
                raise
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body=query,
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                ),
                'fields':fields,
                'limit':limit,
                'skip':skip,
                'sort':sort
            }
        )
        return True
