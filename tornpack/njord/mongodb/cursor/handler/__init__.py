from tornpack.actor.base import Base
from tornpack.njord.mongodb.cursor.close import Close
from tornpack.njord.mongodb.cursor.distinct import Distinct
from tornpack.njord.mongodb.cursor.fetch import Fetch

__all__ = ['Handler']

class Handler(Base):
    __cursor = None
    __service = None

    def __init__(self,service,cursor):
        self.__service = service
        self.__cursor = cursor

    def close(self,future=None):
        def on_close(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        Close().run(
            service=self.__service,
            cursor_id=self.__cursor,
            future=self.ioengine.future_instance(on_close)
        )
        return True

    def distinct(self,key,future):
        Distinct().run(
            service=self.__service,
            cursor_id=self.__cursor,
            key=key,
            future=future
        )
        return True

    def next(self,future):
        Fetch().run(
            service=self.__service,
            cursor_id=self.__cursor,
            future=future
        )
        return True
