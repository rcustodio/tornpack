from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy

__all__ = ['Distinct']

class Distinct(Base):
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_mongodb['cursor']['distinct']

    @property
    def name(self):
        return 'njord_mongodb_cursor_distinct'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['payload_error']:self.__err__,
                self.codes['nok']:self.__nok__,
                self.codes['ok']:self.__ok__
            }
        except:
            raise
        return self.__op_codes

    def __err__(self,msg,future):
        future.set_exception(ValueError())
        return True

    def __nok__(self,msg,future):
        future.set_result(False)
        return True

    def __ok__(self,msg,future):
        future.set_result(dictfy(msg['body']))
        return True

    def run(self,service,cursor_id,key,future):
        def on_ask(result):
            try:
                self.ioengine.ioloop.add_callback(self.op_codes[msg['properties'].headers['code']],msg=result.result(),future=future)
            except KeyError:
                future.set_result(False)
            except:
                raise
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            headers={
                'service':service,
                'action':self.name,
                'cursor_id':cursor_id,
                'key':key
            }
        )
        return True
