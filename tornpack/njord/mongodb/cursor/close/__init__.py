from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options

__all__ = ['Close']

class Close(Base):
    @property
    def name(self):
        return 'njord_mongodb_cursor_close'

    def run(self,service,cursor_id,future):
        def on_ask(result):
            future.set_result(True)
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            headers={
                'service':service,
                'action':self.name,
                'cursor_id':cursor_id
            }
        )
        return None
