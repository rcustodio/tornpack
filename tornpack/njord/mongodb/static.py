__all__ = [
    'mongodb_path'
]

def mongodb_path(env,db,collection,service):
    return '%s_%s.%s.%s' % (env,db,collection,service)
