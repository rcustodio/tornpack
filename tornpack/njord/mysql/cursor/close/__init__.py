from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mysql.static import mysql_path as MySQLPATH
from tornpack.options import options

__all__ = ['Close']

class Close(Base):
    @property
    def name(self):
        return 'njord_mysql_cursor_close'

    @property
    def service(self):
        return 'cursor_close'

    def mysql_path(self,env,db,table):
        return MySQLPATH(env,db,table,self.service)

    def run(self,env,db,table,cursor,future):
        def on_ask(result):
            future.set_result(True)
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            headers={
                'mysql_path':self.mysql_path(
                    env=env,
                    db=db,
                    table=table
                ),
                'cursor':cursor
            }
        )
        return True
