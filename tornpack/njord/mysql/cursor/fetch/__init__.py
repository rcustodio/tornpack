from hashlib import sha1
from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mysql.static import mysql_path as MySQLPATH
from tornpack.options import options
from tornpack.parser.json import dictfy
from .fetch_pb2 import FetchResponse as PBFetchResponse

__all__ = ['Fetch']

class Fetch(Base):
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_mysql['codes']['cursor']['fetch']

    @property
    def name(self):
        return 'njord_mysql_cursor_fetch'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['empty']:self.__op_empty__,
                self.codes['ok']:self.__op_ok__
            }
        except:
            raise
        return self.__op_codes

    @property
    def service(self):
        return 'cursor_fetch'

    def __op_empty__(self,body,future):
        future.set_result(None)
        return True

    def __op_ok__(self,body,future):
        future.set_result(dictfy(body))
        return True

    def mysql_path(self,env,db,table):
        return MySQLPATH(env,db,table,self.service)

    def run(self,env,db,table,cursor,future):
        def on_decode(result):
            self.ioengine.ioloop.add_callback(
                self.op_codes[result.result().code],
                body=result.result().payload,
                future=future
            )
            return True

        def on_ask(result):
            self.ioengine.ioloop.add_callback(
                self.__protobuf_decode__,
                result.result()['body'],
                PBFetchResponse(),
                self.ioengine.future_instance(on_decode)
            )
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            headers={
                'mysql_path':self.mysql_path(
                    env=env,
                    db=db,
                    table=table
                ),
                'cursor':cursor
            }
        )
        return True
