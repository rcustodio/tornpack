from tornpack.actor.base import Base
from tornpack.njord.mysql.cursor.close import Close
from tornpack.njord.mysql.cursor.fetch import Fetch

__all__ = ['Handler']

class Handler(Base):
    __closed = False
    __db = None
    __env = None
    __table = None
    __uid = None

    @property
    def closed(self):
        return self.__closed

    @property
    def db(self):
        return self.__db

    @property
    def env(self):
        return self.__env

    @property
    def table(self):
        return self.__table

    @property
    def uid(self):
        return self.__uid

    def __init__(self,env,db,table,uid):
        self.__env = env
        self.__db = db
        self.__table = table
        self.__uid = uid

    def close(self,future=None):
        try:
            assert not self.closed
        except AssertionError:
            return False
        except:
            raise
        def on_close(result):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                future.set_result(True)
            return True

        self.__closed = True
        Close().run(
            env=self.__env,
            db=self.__db,
            table=self.__table,
            cursor=self.uid,
            future=self.ioengine.future_instance(on_close)
        )
        return True

    def next(self,future):
        try:
            assert not self.closed
        except AssertionError:
            future.set_exception(StopIteration())
            return True
        except:
            raise

        def on_fetch(result):
            try:
                assert result.result()
            except AssertionError:
                future.set_exception(StopIteration())
            except:
                raise
            else:
                future.set_result(result.result())
            return True

        Fetch().run(
            env=self.__env,
            db=self.__db,
            table=self.__table,
            cursor=self.uid,
            future=self.ioengine.future_instance(on_fetch)
        )
        return True
