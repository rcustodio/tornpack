from hashlib import sha1
from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mysql.exceptions import DecodeException,QueryInvalid
from tornpack.njord.mysql.static import mysql_path as MySQLPATH
from tornpack.options import options
from tornpack.parser.json import jsonify
from .update_pb2 import Update as PBUpdate,UpdateResponse as PBUpdateResponse

__all__ = ['Update']

class Update(Base):
    __etag = None
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_mysql['codes']['update']

    @property
    def etag(self):
        try:
            assert self.__etag
        except AssertionError:
            self.__etag = sha1('%s:%s<%s>' % (self.name,self.ioengine.uuid4,self.ioengine.uuid4)).hexdigest()
        except:
            raise
        return self.__etag

    @property
    def name(self):
        return 'njord_mysql_update'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['decode_error']:self.__op_decode_error__,
                self.codes['nok']:self.__op_nok__,
                self.codes['ok']:self.__op_ok__,
                self.codes['query_error']:self.__op_query_error__
            }
        except:
            raise
        return self.__op_codes

    @property
    def service(self):
        return 'update'

    def __op_decode_error__(self,future):
        future.set_exception(DecodeException())
        return True

    def __op_nok__(self,future):
        future.set_result(False)
        return True

    def __op_ok__(self,future):
        future.set_result(True)
        return True

    def __op_query_error__(self,future):
        future.set_exception(QueryInvalid())
        return True

    def mysql_path(self,env,db,table):
        return MySQLPATH(env,db,table,self.service)

    def run(self,env,db,table,cond,document,future):
        def on_decode(result):
            self.ioengine.ioloop.add_callback(
                self.op_codes[result.result().code],
                future=future
            )
            return True

        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.__protobuf_decode__,
                    result.result()['body'],
                    PBUpdateResponse(),
                    self.ioengine.future_instance(on_decode)
                )
            return True

        Njord.publish(
            name=self.name,
            body=PBUpdate(
                etag=self.etag,
                env=env,
                cond=cond,
                payload=jsonify(document)
            ),
            future=self.ioengine.future_instance(on_ask),
            headers={
                'mysql_path':self.mysql_path(
                    env=env,
                    db=db,
                    table=table
                )
            }
        )
        return True
