__all__ = [
    'MySQLException',
    'DecodeException',
    'EncodeException',
    'QueryInvalid'
]

class MySQLException(Exception):
    pass

class DecodeException(MySQLException):
    pass

class EncodeException(MySQLException):
    pass

class QueryError(MySQLException):
    pass

class QueryInvalid(MySQLException):
    pass
