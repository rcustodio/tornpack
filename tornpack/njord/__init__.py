from kernel import Kernel as Njord
from tornpack.options import options

__all__ = ['Njord']

try:
    assert options.tornpack_njord
except (AssertionError,AttributeError):
    pass
except:
    raise
else:
    Njord = Njord()
