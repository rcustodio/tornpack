from hashlib import sha1
from tornpack.options import options

__all__ = ['routing_key']

def routing_key(_):
    return options.tornpack_actor_rmq_rk % (sha1(_).hexdigest())
