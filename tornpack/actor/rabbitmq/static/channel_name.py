from hashlib import sha1

__all__ = ['channel_name']

def channel_name(name,uid):
    return sha1('%s:%s' % (name,uid)).hexdigest()

