from channel_name import channel_name
from queue_name import queue_name
from routing_key import routing_key

__all__ = [
    'channel_name',
    'queue_name',
    'routing_key'
]
