from tornpack.actor.base import Base
from .engine import Engine
from .engines import ENGINES

__all__ = ['Kernel']

class Kernel(Base):
    __engine__ = Engine

    @property
    def engines(self):
        return ENGINES

    def engine_close(self,name,future=None):
        def on_connection(connection):
            connection.result().close()
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            return True

        try:
            self.engines[name].connection(self.ioengine.future_instance(on_connection))
        except KeyError:
            pass
        except:
            raise
        else:
            del self.engines[name]
        return True

    def engine(self,name,future):
        try:
            assert self.engines[name]
        except (AssertionError,KeyError):
            self.engines[name] = self.__engine__()
        except:
            raise

        self.engines[name].connection(future)
        return True

    def engine_open(self,name,url,future=None):
        def on_close(engine):
            self.ioengine.ioloop.add_callback(self.engine_open,name,url)
            return True

        try:
            assert self.engines[name]
        except (AssertionError,KeyError):
            self.engines[name] = self.__engine__()
        except:
            raise

        self.engines[name].on_close(self.ioengine.future_instance(on_close))
        self.engines[name].run(name=name,url=url)
        self.engines[name].connection(future)
        return True

Kernel = Kernel()
