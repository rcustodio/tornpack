from tornpack.actor.base import Base

__all__ = ['Queue']

class Queue(Base):
    def bind(self,channel,queue,exchange,routing_key,arguments=None,future=None):
        def on_bind(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.queue_bind(queue=queue,exchange=exchange,routing_key=routing_key,arguments=arguments,callback=on_bind)
        return True

    def declare(self,channel,queue,durable=False,exclusive=True,auto_delete=True,future=None,arguments={}):
        def on_declare(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.queue_declare(queue=queue,durable=durable,exclusive=exclusive,auto_delete=auto_delete,callback=on_declare,arguments=arguments)
        return True

    def delete(self,channel,queue,if_unused=False,if_empty=False,future=None):
        def on_delete(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.queue_delete(queue=queue,if_unused=if_unused,if_empty=if_empty,callback=on_delete)
        return True

    def purge(self,channel,queue,future=None):
        def on_purge(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.queue_purge(queue=queue,callback=on_purge)
        return True

    def unbind(self,channel,queue,exchange,routing_key,arguments=None,future=None):
        def on_unbind(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.queue_unbind(queue=queue,exchange=exchange,routing_key=routing_key,arguments=arguments,callback=on_unbind)
        return True

Queue = Queue()
