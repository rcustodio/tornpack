from tornpack.actor.base import Base

__all__ = ['Consumer'] 

class Consumer(Base):
    def cancel(self,channel,consumer_tag,nowait=False,future=None):
        def on_cancel(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True
            
        channel.basic_cancel(consumer_tag=consumer_tag,callback=on_cancel)
        return True
    
    def consume(self,channel,consumer_callback,queue,no_ack=False,exclusive=False,consumer_tag=None,arguments=None):
        channel.basic_consume(consumer_callback=consumer_callback,
                              queue=queue,
                              no_ack=no_ack,
                              exclusive=exclusive,
                              consumer_tag=consumer_tag,
                              arguments=arguments) 
        return True

Consumer = Consumer()
