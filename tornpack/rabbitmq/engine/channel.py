from pika import exceptions
from tornado.gen import engine
from tornpack.actor.base import Base

__all__ = ['Channel']

class Channel(Base):
    __close_callbacks = None
    __closed = False
    __channel = None
    __name = None
    __prefetch_count = None
    __running = False

    @property
    def close_callbacks(self):
        try:
            assert self.__close_callbacks
        except AssertionError:
            self.__close_callbacks = []
        except:
            raise
        return self.__close_callbacks

    @property
    def name(self):
        return self.__name

    @property
    def prefetch_count(self):
        return self.__prefetch_count

    @prefetch_count.setter
    def prefetch_count(self,arg):
        self.__prefetch_count = arg
        self.ioengine.ioloop.add_callback(self.__qos_set)
        return True

    def __close(self):
        try:
            assert self.is_ready
            assert self.__channel.is_open
            assert self.__connection.is_open
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__channel.close()
        return True

    def __open(self,future):
        def on_close(*args):
            self.ioengine.ioloop.add_callback(self.__resolve_close_callbacks)
            return True

        def on_connect(channel):
            self.is_ready = channel
            self.__channel = channel
            channel.add_on_close_callback(on_close)

            try:
                future.set_result(channel)
            except AttributeError:
                pass
            except:
                raise
            return True

        self.__connection.channel(on_connect)
        return True

    def __qos_set(self):
        try:
            assert self.__channel
            self.__channel.basic_qos(prefetch_count=self.prefetch_count)
        except AssertionError:
            def on_channel(channel):
                self.ioengine.ioloop.add_callback(self.__qos_set)
                return True

            self.promises.append(self.ioengine.future_instance(on_channel))
        except exceptions.ChannelClosed:
            pass
        except:
            raise
        return True

    def __resolve_close_callbacks(self):
        try:
            self.close_callbacks.pop(0).set_result(True)
        except IndexError:
            pass
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__resolve_close_callbacks)
        return True

    def __init__(self,name=None,prefetch_count=10):
        self.__name = name or self.ioengine.uuid4
        self.prefetch_count = prefetch_count

    def make(self,connection,future=None):
        try:
            assert not self.__running
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__running = True
            self.__connection = connection
            self.ioengine.ioloop.add_callback(self.__open,future=future)
        return True

    def close(self):
        try:
            assert not self.__closed
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__closed = True
            self.ioengine.ioloop.add_callback(self.__close)
        return True

    def on_close(self,future):
        self.close_callbacks.append(future)
        return True
