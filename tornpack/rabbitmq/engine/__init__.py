from tornado.concurrent import Future
from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.rabbitmq.engines import ENGINES
from .connection import Connection
from .channel import Channel
from .consumer import Consumer
from .exchange import Exchange
from .publish import Publish
from .queue import Queue

__all__ = ['Engine']

class Engine(Base):
    __channels = None
    __channels_future = None
    __close_callbacks = None
    __closed = False
    __connection = None
    __running = False
    name = None
    url = None

    @property
    def channels(self):
        try:
            assert self.__channels
        except AssertionError:
            self.__channels = {}
        except:
            raise
        return self.__channels

    @property
    def close_callbacks(self):
        try:
            assert self.__close_callbacks
        except AssertionError:
            self.__close_callbacks = []
        except:
            raise
        return self.__close_callbacks

    @property
    def connection_io(self):
        return self.is_ready

    @connection_io.setter
    def connection_io(self,_):
        self.is_ready = _

    @property
    def consumer(self):
        return Consumer

    @property
    def exchange(self):
        return Exchange

    @property
    def is_open(self):
        try:
            return self.__connection.is_open
        except AttributeError:
            pass
        except:
            raise
        return False

    @property
    def queue(self):
        return Queue

    @property
    def publish(self):
        return Publish

    @property
    def running(self):
        return self.__running

    def __close_channels(self,channels=None):
        try:
            channels.next().close()
        except AttributeError:
            self.ioengine.ioloop.add_callback(self.__close_channels,channels=iter(self.channels.values()))
            self.__channels = None
        except StopIteration:
            self.ioengine.ioloop.add_callback(self.__resolve_close_callbacks)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__close_channels,channels=channels)
        return True

    def __connect(self):
        def on_connect(result):
            self.__connection = result.result()
            self.__connection.add_on_close_callback(self.__on_disconnect)
            self.is_ready = self
            return True

        Connection.run(msg={'url':self.url},future=self.ioengine.future_instance(on_connect))
        return True

    def __on_disconnect(self,connection,*args):
        try:
            assert not self.__closed
        except AssertionError:
            pass
        except:
            raise
        else:
            del ENGINES[self.name]
            self.ioengine.ioloop.add_callback(self.__close_channels)
        return True

    def __resolve_close_callbacks(self):
        try:
            self.close_callbacks.pop().set_result(self)
        except IndexError:
            pass
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__resolve_close_callbacks)
        return True

    def channel(self,name,future,prefetch_count=10):
        try:
            assert self.channels[name].is_ready
        except AssertionError:
            self.channels[name].promises.append(future)
        except KeyError:
            self.channels[name] = Channel(
                name=name,
                prefetch_count=prefetch_count
            )
            def on_close(result):
                try:
                    del self.channels[name]
                except KeyError:
                    pass
                except:
                    raise
                return True

            def on_connection(connection):
                self.channels[name].run(
                    connection=self.__connection,
                    future=future
                )
                return True

            self.channels[name].on_close(self.ioengine.future_instance(on_close))
            self.connection(self.ioengine.future_instance(on_connection))
        except:
            raise
        else:
            future.set_result(self.channels[name].io)
        return True

    def close(self):
        try:
            assert not self.__closed
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__closed = True
            self.__connection.close()
        return True

    def connection(self,future):
        try:
            assert self.is_ready
            assert self.__connection.is_open
        except AssertionError:
            self.promises.append(future)
        except:
            raise
        else:
            future.set_result(self)
        return True

    def make(self,**kwargs):
        try:
            assert not self.__running
            self.name = kwargs['name']
            self.url = kwargs['url']
        except AssertionError:
            return False
        except KeyError:
            raise
        except:
            raise
        else:
            self.__running = True
            self.ioengine.ioloop.add_callback(self.__connect)
        return True

    def on_close(self,future):
        self.close_callbacks.append(future)
        return True
