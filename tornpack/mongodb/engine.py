from tornpack.actor.base import Base
from tornpack.options import options
from .client import Client
from .driver import Driver

__all__ = ['Engine']

class Engine(Base):
    __connection = None

    def client(self,db,collection,future):
        def on_connection(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,
                                                  msg={'future':future,
                                                       'swap':{'future':Client(db=db,
                                                                               collection=collection,
                                                                               io=self.__connection)}})
            return True
        
        self.connection(self.ioengine.future_instance(on_connection))
        return True
    
    def connect(self,url):
        def on_connect(result):
            try:
                assert result.result()
            except AssertionError:
                self.ioengine.ioloop.add_callback(self.connect,url=url)
            except:
                raise
            else:
                self.__connection = result.result()
                self.is_ready = self
            return True

        Driver.connect(url,self.ioengine.future_instance(on_connect))
        return True

    def connection(self,future):
        try:
            assert self.__connection
        except AssertionError:
            self.promises.append(future)
        except:
            raise
        else:
            future.set_result(self)
        return True
