from .dhtdb import DHTDB
from .uprofile import Uprofile
from .uuid import UUID
from .welcome import Welcome

__all__ = ['actions']

actions = {
    'dhtdb':DHTDB().on_msg,
    'uprofile':Uprofile().on_msg,
    'uuid':UUID().init,
    'welcome':Welcome().on_msg
}
