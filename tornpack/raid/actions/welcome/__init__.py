from iam import IAM
from tornpack.actor.base import Base
from who import Who

__all__ = ['Welcome']

class Welcome(Base):
    __actions = {
        'iam':IAM().init,
        'who':Who().init
    }

    @property
    def actions(self):
        return self.__actions
    
    def on_msg(self,msg,io):
        try:
            assert msg['header']['method'] in self.actions
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.actions[msg['header']['method']],msg=msg,io=io)
        return True
