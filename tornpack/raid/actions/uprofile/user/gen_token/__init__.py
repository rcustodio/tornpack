from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.options import options
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['GenToken']

class GenToken(SimpleAsk):
    def __acl_error__(self,io,etag):
        io.error(
            etag=etag,
            code=options.tornpack_raid_codes['acl']['unauthorized']
        )
        return True

    def __define_parse_payload__(self):
        try:
            assert options.debug
        except AssertionError:
            def parse(msg):
                try:
                    assert msg
                except AssertionError:
                    pass
                except:
                    raise
                else:
                    return {
                        'client_token':msg['client_token'],
                        'type':msg['type'],
                        'user':msg['user']
                    }
                return False
        except:
            raise
        else:
            def parse(msg):
                try:
                    assert msg
                except AssertionError:
                    pass
                except:
                    raise
                else:
                    return {
                        'client_token':msg['client_token'],
                        'type':msg['type'],
                        'user':msg['user'],
                        'user_token':msg['user_token']
                    }
                return False

        self.__parse_payload__ = parse
        return True

    def __init__(self):
        self.__define_parse_payload__()

    def ask(self,msg,io):
        def on_tell(result):
            try:
                io.push({
                    'body':self.__parse_payload__(dictfy(result.result()['body'])),
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify(msg['body']),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['user']['gen_token'])
        )
        return True

    def app_auth(self,msg,io):
        try:
            assert io.app
        except AssertionError:
            self.__acl_error__(io,msg['header']['etag'])
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['user']
            assert msg['body']['type'] in options.tornpack_uprofile_user_types
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.app_auth,msg=msg,io=io)
        return True
