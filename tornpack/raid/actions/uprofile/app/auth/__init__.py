from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Auth']

class Auth(SimpleAsk):
    def ask(self,msg,io):
        def on_tell(result):
            try:
                io.push({
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.check_result,
                    result=result.result(),
                    msg=msg,
                    io=io
                )
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify(msg['body']),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['app']['auth']),
        )
        return True

    def check_result(self,result,msg,io):
        try:
            assert result['properties'].headers['code'] == options.tornpack_uprofile_rabbitmq['codes']['app']['auth']['ok']
        except AssertionError:
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            io.app_auth(msg['body']['name'])
        return True

    def is_auth(self,msg,io):
        try:
            assert io.app
        except AssertionError:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(io.disconnect)
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['name']
            assert msg['body']['token']
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.is_auth,msg=msg,io=io)
        return True
