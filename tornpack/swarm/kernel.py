from tornpack.actor.base import Base

__all__ = ['Kernel']

class Kernel(Base):
    __actor = None
    __zone = None

    @property
    def actor(self):
        try:
            assert self.__actor
        except AssertionError:
            self.__actor = {}
        except:
            raise
        return self.__actor
    
    @property
    def zone(self):
        try:
            assert self.__zone
        except AssertionError:
            self.__zone = {}
        except:
            raise
        return self.__zone

    def __actor_add(self,actor):
        try:
            self.actor[actor.name] = actor
        except:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
            raise

        self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        return True

    def __actor_del(self,actor):
        try:
            del self.actor[actor.name]
        except:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
            raise

        self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        return True
    
    def __zone_add(self,name,actor,future):
        try:
            assert name in self.zone
        except AssertionError:
            self.zone[name] = {actor.name:actor}
        except:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
            raise
        else:
            self.zone[name][actor.name] = actor

        self.ioengine.ioloop.add_callback(self.actor_add,actor=actor)
        self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        return True

    def __zone_del(self,name,future,bucket=None):
        try:
            actor = bucket.next()
        except AttributeError:
            self.ioengine.ioloop.add_callback(self.__zone_del,name=name,future=future,bucket=iter(self.zone[name]))
        except StopIteration:
            del self.zone[name]
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.actor_del,actor=actor)
            self.ioengine.ioloop.add_callback(self.__zone_del,name=name,future=future,bucket=bucket)
        return None
    
    def actor_add(self,actor,future=None):
        def on_add(result):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':result.result()}})
            return True

        self.ioengine.ioloop.add_callback(self.__actor_add,actor=actor,future=self.ioengine.future_instance(on_add))
        return True

    def actor_del(self,actor,future=None):
        def on_del(result):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':result.result()}})
            return True
        
        self.ioengine.ioloop.add_callback(self.__actor_del,actor=actor,future=self.ioengine.future_instance(on_del))
        return True
    
    def zone_add(self,name,actor,future=None):
        def on_add(result):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':result.result()}})
            return True

        self.ioengine.ioloop.add_callback(self.__zone_add,name=name,actor=actor,future=self.ioengine.future_instance(on_add))
        return True

    def zone_del(self,name,future=None):
        def on_del(result):
            try:
                assert future
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':result.result()}})
            return True

        self.ioengine.ioloop.add_callback(self.__zone_del,name=name,future=self.ioengine.future_instance(on_del))
        return True
