test:
	python test.py

config: git-pre-commit pip fix-google-protobuf
	touch .services
	python make.py

bash:
	echo "export PYTHONPATH=.pip" >> $$HOME/.bash_profile

tcsh:
	echo "setenv PYTHONPATH .pip" >> $$HOME/.tcshrc

fix-google-protobuf:
	touch .pip/google/__init__.py

git-pre-commit:
	cp .git/hooks/pre-commit.sample .git/hooks/pre-commit
	echo -e "#!/bin/sh\nmake test || exit 1" > .git/hooks/pre-commit

pip:
	pip install -r require.txt -t .pip --upgrade

purge:
	rm -rf *
	rm -rf .pip .services
	git checkout .

service-clean:
	touch .services

service-declare:
	echo $R >> .services

service-install:
	git clone git@gitlab.com:$$O/$$R.git
	echo $R >> .services

uuid:
	python -c "from uuid import uuid4 ; print uuid4().hex"
