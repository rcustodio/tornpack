from tornpack.actor.base import Base
from tornpack.ioengine import IOEngine
from tornpack.testing import AsyncTestCase

__all__ = ['TestBase']

class TestBase(AsyncTestCase):
    def test__future_after__must_do_nothing(self):
        self.assertFalse(Base().__future_after__())

    def test_future(self):
        def on_future(result):
            IOEngine.ioloop.stop()
        
        Base().future(msg={
            'swap':{'future':True},
            'future':IOEngine.future_instance(on_future)
        })
        IOEngine.ioloop.start()

    def test_future_calling_future_after(self):
        def on_future(msg):
            IOEngine.ioloop.stop()
        
        b = Base()
        b.__future_after__ = on_future        
        b.future(msg={})
        IOEngine.ioloop.start()
        
    def test_make_must_be_empty(self):
        self.assertFalse(Base().make())

    def test_run_must_call_make(self):      
        def nmake(**kwargs):
            IOEngine.ioloop.stop()
        
        b = Base()
        b.make = nmake
        b.run()
        IOEngine.ioloop.start()
        
    def test_schema_must_be_array(self):
        self.assertEquals(Base().schema,[])

    def test_schema(self):
        schema = ['some','value','here']
        b = Base()
        b.schema = schema

        self.assertEquals(b.schema,schema)
        
    def test_uid_must_return_uid_from_ioengine(self):
        b = Base()
        self.assertTrue(b.uid)
        self.assertEquals(b.uid,b.uid)
        self.assertNotEquals(b.uid,b.ioengine.uuid4)

    def test_validate_with_nothing(self):
        def on_validate(**kwargs):
            IOEngine.ioloop.stop()
        
        b = Base()
        b.validate_ok = on_validate
        b.validate(msg={'swap':{'envelop':{}},'msg':{}})
        IOEngine.ioloop.start()

    def test_validate_false(self):
        def on_validate(result):
            IOEngine.ioloop.stop()
        
        b = Base()
        b.schema = ['some']
        b.validate(msg={'swap':{'envelop':{}},'msg':{'foo':'bar'},'future':IOEngine.future_instance(on_validate)})
        IOEngine.ioloop.start()
        
    def test_validate_true(self):
        def on_validate(**kwargs):
            IOEngine.ioloop.stop()

        b = Base()
        b.schema = ['name']
        b.validate_ok = on_validate
        b.validate(msg={'swap':{'envelop':{}},'msg':{'name':'asd'}})
        IOEngine.ioloop.start()
        
    def test_validate_ok_must_return_none(self):
        self.assertEquals(Base().validate_ok,None)
