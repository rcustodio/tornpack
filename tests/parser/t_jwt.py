import jwt
from tornpack.parser.jwt import decode, encode
from tornpack.testing import AsyncTestCase

__all__ = ['JWT']

class JWT(AsyncTestCase):
    __payload = {"name":"Umgeher"}
    __secret = "tornpack"

    def test_decode(self):
        encoded = encode(self.__payload,self.__secret)
        self.assertEquals(
            decode(encoded,self.__secret),
            jwt.decode(encoded,self.__secret)
        )
    
    def test_encode(self):
        self.assertEquals(
            encode(self.__payload,self.__secret),
            jwt.encode(self.__payload,self.__secret,algorithm='HS256')
        )
