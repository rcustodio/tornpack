from tornpack.njord.mysql.cursor.handler import Handler
from tornpack.testing import AsyncTestCase

__all__ = ['TestCursorHandler']

class TestCursorHandler(AsyncTestCase):
    def test__init_(self):
        env = 'test'
        db = 'dbtest'
        table = 'tabletest'
        uid = '123456'

        cursor = Handler(
            env=env,
            db=db,
            table=table,
            uid=uid
        )

        self.assertEquals(env,cursor.env)
        self.assertEquals(db,cursor.db)
        self.assertEquals(table,cursor.table)
        self.assertEquals(uid,cursor.uid)

    def test__init__without_env(self):
        self.assertRaises(TypeError,Handler,db='dbtest',table='tabletest',uid='123456')

    def test__init__without_db(self):
        self.assertRaises(TypeError,Handler,env='test',table='tabletest',uid='123456')

    def test__init__without_table(self):
        self.assertRaises(TypeError,Handler,env='test',db='dbtest',uid='123456')

    def test__init__without_uid(self):
        self.assertRaises(TypeError,Handler,env='test',db='dbtest',table='tabletest')

    def test_next_without_future(self):
        self.assertRaises(TypeError,Handler('test','dbtest','tabletest','123456').next)
