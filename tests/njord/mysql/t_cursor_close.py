from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.cursor.close import Close
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestCursorClose']

class TestCursorClose(AsyncTestCase):
    def test_mysql_path(self):
        obj = Close()
        self.assertEquals(obj.mysql_path('test','dbtest','dbtable'),mysql_path('test','dbtest','dbtable',obj.service))

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Close().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Close().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Close().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_cursor_close',Close().name)

    def test_service(self):
        self.assertEquals('cursor_close',Close().service)
