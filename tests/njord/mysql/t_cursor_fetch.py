from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.cursor.fetch import Fetch
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestCursorFetch']

class TestCursorFetch(AsyncTestCase):
    def test__op_empty__(self):
        def on_call(result):
            self.assertEquals(None,result.result())
            IOEngine.ioloop.stop()

        Fetch().__op_empty__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_empty__without_data(self):
        self.assertRaises(TypeError,Fetch().__op_empty__,future=None)

    def test__op_empty__without_future(self):
        self.assertRaises(TypeError,Fetch().__op_empty__,data=None)

    def test__op_ok__(self):
        json = {'a':2}
        def on_call(result):
            self.assertEquals(json,result.result())
            IOEngine.ioloop.stop()

        Fetch().__op_ok__(jsonify(json),IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_ok__without_data(self):
        self.assertRaises(TypeError,Fetch().__op_ok__,future=None)

    def test__op_ok__without_future(self):
        self.assertRaises(TypeError,Fetch().__op_ok__,data=None)

    def test_mysql_path(self):
        obj = Fetch()
        self.assertEquals(obj.mysql_path('test','dbtest','dbtable'),mysql_path('test','dbtest','dbtable',obj.service))

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Fetch().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Fetch().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Fetch().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_cursor_fetch',Fetch().name)

    def test_service(self):
        self.assertEquals('cursor_fetch',Fetch().service)
