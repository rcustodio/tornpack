from tornpack.njord.mysql.static import mysql_path
from tornpack.testing import AsyncTestCase

__all__ = ['TestStatic']

class TestStatic(AsyncTestCase):
    def test_mysql_path(self):
        self.assertEquals('test.dbtest.tabletest.async_test',mysql_path('test','dbtest','tabletest','async_test'))

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,mysql_path,db='dbtest',table='tabletest',service='async_test')

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,mysql_path,env='test',table='tabletest',service='async_test')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,mysql_path,env='test',db='dbtest',service='async_test')

    def test_mysql_path_without_service(self):
        self.assertRaises(TypeError,mysql_path,env='test',db='dbtest',table='tabletest')
