from tests import tests
from tornpack.testing import main, unittest

def all():
    return unittest.defaultTestLoader.loadTestsFromNames(tests)

main(verbosity=2)
